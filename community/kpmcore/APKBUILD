# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kpmcore
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/system/org.kde.partitionmanager"
pkgdesc="Library for managing partitions"
license="GPL-3.0-or-later"
depends="
	device-mapper-udev
	sfdisk
	smartmontools
	"
makedepends="
	extra-cmake-modules
	kauth-dev
	kcoreaddons-dev
	ki18n-dev
	kwidgetsaddons-dev
	qca-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kpmcore-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # Requires running dbus server

# secfixes:
#   4.2.0-r0:
#     - CVE-2020-27187

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3671065ee530f13ebb76d25db5cba1cd62f89df847a070d994a3070412d9cd23ae4bf8befe249f5437b925c901e851ce97202c91ce59172a6fb4770841c26c0f  kpmcore-22.12.0.tar.xz
"
