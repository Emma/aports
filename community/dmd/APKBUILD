# Contributor: Mathias LANG <pro.mathias.lang@gmail.com>
# Maintainer: Mathias LANG <pro.mathias.lang@gmail.com>
pkgname=dmd
pkgver=2.101.0
pkgrel=0
pkgdesc="D Programming Language reference compiler"
url="https://github.com/dlang/dmd"
# TODO: Enable on x86 once LDC-1.20.1 is out (and enabled on x86)
arch="x86_64"
license="BSL-1.0"
depends="llvm-libunwind-dev tzdata"
makedepends="chrpath dtools ldc"
source="dmd-$pkgver.tar.gz::https://github.com/dlang/dmd/archive/v$pkgver.tar.gz
	phobos-$pkgver.tar.gz::https://github.com/dlang/phobos/archive/v$pkgver.tar.gz

	dmd-install-config.conf
	10-dmd-musl.patch
	"
builddir="$srcdir"

prepare() {
	# The Makefiles make some assumption about the directory structure
	ln -s "$srcdir/dmd-$pkgver/" "$srcdir/dmd"
	ln -s "$srcdir/phobos-$pkgver/" "$srcdir/phobos"

	default_prepare
}

build() {
	./dmd/compiler/src/build.d dmd HOST_DMD="ldmd2"
	make -C "dmd/druntime/" -f posix.mak ENABLE_RELEASE=1 INSTALL_DIR="$srcdir/install" install
	make -C "phobos/" -f posix.mak ENABLE_RELEASE=1 INSTALL_DIR="$srcdir/install" install
	./dmd/compiler/src/build.d unittest HOST_DMD="dmd/generated/linux/release/64/dmd"

	# Strip redundant rpath to avoid warnings in the builder
	chrpath -d "dmd/generated/linux/release/64/dmd"
}

check() {
	dmd/generated/linux/release/64/dmd-unittest
}

package() {
	mkdir -p "$pkgdir/usr/bin/" "$pkgdir/usr/lib/" "$pkgdir/etc/" "$pkgdir/usr/include/dmd/"

	mv dmd/generated/linux/release/64/dmd "$pkgdir"/usr/bin/dmd
	cp dmd-install-config.conf "$pkgdir"/etc/dmd.conf
	mv install/linux/lib64/* "$pkgdir"/usr/lib/

	mv install/src/druntime/import/ "$pkgdir"/usr/include/dmd/druntime
	mv install/src/phobos/ "$pkgdir"/usr/include/dmd/phobos/
}
sha512sums="
52d10adbc146f912abbaa0e7813f845f2374ceb79cb2a1f339bd6040f1ccdcce501e8fe802c44ba46a1356f92ba37322a0700b8fc9a4d0f81a35dd5fa06f0c16  dmd-2.101.0.tar.gz
31a4e7bee92c63e5e05361e6e0c65dfaf83d8f56acd1e2c7ccedbe9b0d192f30ae924ee1c9321f35ba0c171866915f8ef89ec14b29b9334f7953112eb9189f4f  phobos-2.101.0.tar.gz
123ec0f256a73030a5e5b4b87a7f2e0752320777b7fcd175a221807ec2917f5d6d88776c3448eab077eb7a2211dd4a3d64e3a556053b0f183eb058da437bc5da  dmd-install-config.conf
928874c8a6acc593f2ac54b785ff551bc16b53ec647c4c7e19b5f19d609f02b200e550d1ee3d024bf969ef417b705c3448ce590adbe9a113a03e9372718a0f55  10-dmd-musl.patch
"
