# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kontrast
pkgver=22.12.0
pkgrel=0
pkgdesc="Tool to check contrast for colors that allows verifying that your colors are correctly accessible"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://invent.kde.org/accessibility/kontrast"
license="GPL-3.0-or-later AND CC0-1.0"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kontrast-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d8d47caea797900d83d51a759bde836670e3380003f964ac2a805f4b1a1ad508ae9fe18e22797330411fdce8556cbe0f87ba1f05f7d2c6a878f0677eb70c6309  kontrast-22.12.0.tar.xz
"
