# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-sdk
pkgver=5.26.4
pkgrel=0
pkgdesc="Applications useful for Plasma Development"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="
	kirigami2
	qt5-qtquickcontrols
	"
makedepends="
	extra-cmake-modules
	karchive-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kparts-dev
	kservice-dev
	ktexteditor-dev
	kwidgetsaddons-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtsvg-dev
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-sdk-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# iconmodeltest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "iconmodeltest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1a683909e9b11acd1356f74a3e34d37d8bd52fed6f49fd4e1d68ec45c28973336a0fb7e91d3d0baae145072c0bdb8faf981e55ab8c82c0dc4fabf5e1fff25af2  plasma-sdk-5.26.4.tar.xz
"
