# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
# Contributor: Antoine Martin (ayakael) <dev@ayakael.net>

pkgname=dotnet7-stage0
pkgver=7.0.100
pkgrel=2

[ "$CBUILD" != "$CHOST" ] && _cross="-$CARCH" || _cross=""

# Tag of tarball generator.
_gittag=v7.0.100-rtm.22521.12

# Versions of prebuilt artifacts and bootstrap tar
_artifactsver=7.0.100
_bootstrapver="7.0.100"
_installerver=$_bootstrapver

# Version of packages that aren't defined in git-info or badly defined
_iltoolsver=7.0.0
_aspnetver=7.0.0

# Patches to be used. String before '_' refers to repo to patch
# Look for patch notes within each patch for what they fix / where they come from
# build_* patches applies directly to $builddir
_patches="
	aspnetcore_use-linux-musl-crossgen-on-non-x64.patch
	build_set-local-repo.patch
	installer_2780-reprodicible-tarball.patch
	installer_runtimepacks.patch
	roslyn_allow-extra-params.patch
	runtime_76500-mono-musl-support.patch
	runtime_76500-properly-set-toolchain-for-alpine.patch
	runtime_77269-fix-opcheckthis.patch
	runtime_77270-fixed-fsharp-crash-issue.patch
	runtime_77364-mono-createdirectory-workaround.patch
	runtime_77682-mono-cgroup-min-memory.patch
	runtime_78146-fixed-clang15-build-issue.patch
	runtime_make-lld-use-depend-on-existing-on-target.patch
	runtime_non-portable-distrorid-fix-alpine.patch
	sdk_dummyshim-fix.patch
	"
_extra_nupkgs="https://globalcdn.nuget.org/packages/stylecop.analyzers.1.2.0-beta.435.nupkg"

_pkgver_macro=${pkgver%.*}
_pkgver_prior=1
_pkgver_name="${_pkgver_macro//[.0]}"
pkgdesc="The .NET Core stage0 bits for dotnet build"
# x86: blocked by https://github.com/dotnet/runtime/issues/77667
# armhf: blocked by https://github.com/dotnet/runtime/issues/77663
# riscv64: port WIP https://github.com/dotnet/runtime/issues/36748
arch="all !x86 !armhf !riscv64"
url=https://dotnet.microsoft.com
license="MIT"
options="!check" # Testsuite in main -build aport
subpackages="
	dotnet$_pkgver_name-stage0-artifacts:artifacts:noarch
	dotnet$_pkgver_name-stage0-bootstrap
	"
source="
	https://repo.gpg.nz/apk/archives/dotnet-${_gittag/release\/}.tar.xz
	dotnet-sdk-$_bootstrapver-linux-musl-x64.noextract::https://dotnetcli.azureedge.net/dotnet/Sdk/$_bootstrapver/dotnet-sdk-$_bootstrapver-linux-musl-x64.tar.gz
	dotnet-sdk-$_bootstrapver-linux-musl-arm64.noextract::https://dotnetcli.azureedge.net/dotnet/Sdk/$_bootstrapver/dotnet-sdk-$_bootstrapver-linux-musl-arm64.tar.gz
	dotnet-sdk-$_bootstrapver-linux-musl-arm.noextract::https://dotnetcli.azureedge.net/dotnet/Sdk/$_bootstrapver/dotnet-sdk-$_bootstrapver-linux-musl-arm.tar.gz
	dotnet-sdk-$_bootstrapver-linux-musl-s390x-2.noextract::https://repo.gpg.nz/apk/archives/dotnet-sdk-$_bootstrapver-linux-musl-s390x.tar.gz
	dotnet-sdk-$_bootstrapver-linux-musl-ppc64le.noextract::https://repo.gpg.nz/apk/archives/dotnet-sdk-$_bootstrapver-linux-musl-ppc64le.tar.gz
	Private.SourceBuilt.Artifacts.$_artifactsver.noextract::https://dotnetcli.azureedge.net/source-built-artifacts/assets/Private.SourceBuilt.Artifacts.$_artifactsver.tar.gz
	$_extra_nupkgs
	$_patches
	"

makedepends_host="
	alpine-release
	autoconf
	automake
	bash
	binutils
	clang
	cmake
	findutils
	g++
	gcc
	grep
	icu-dev
	krb5-dev
	libintl
	libstdc++
	libucontext-dev
	libunwind-dev
	libxml2-dev
	libxml2-utils
	linux-headers
	lttng-ust-dev
	musl-dev
	musl-utils
	openssl-dev
	pigz
	unzip
	zip
	zlib-dev
	"
case $CARCH in
	s390x|x86) ;;
	*) makedepends_host="$makedepends_host lld-dev";;
esac
makedepends_build="
	$makedepends_host
	binutils$_cross
	git
	gcc$_cross
	llvm
	llvm-dev
	python3
	xz
	"
case $CBUILD_ARCH in
	x86_64) _dotnet_arch="x64";;
	aarch64) _dotnet_arch="arm64";;
	armv7|armhf) _dotnet_arch="arm";;
	i586) _dotnet_arch="x86";;
	*) _dotnet_arch=$CBUILD_ARCH;;
esac
case $CTARGET_ARCH in
	x86_64) _dotnet_target="x64";;
	aarch64) _dotnet_target="arm64";;
	armv7|armhf) _dotnet_target="arm";;
	i586) _dotnet_target="x86";;
	*) _dotnet_target=$CTARGET_ARCH;;
esac

builddir="$srcdir"/dotnet-${_gittag/release\/}
_packagesdir="$srcdir"/local-packages
_libdir="/usr/lib"
_nugetdir="$srcdir"/nuget
_downloaddir="$srcdir"/local-downloads
_cli_root="$srcdir"/bootstrap
_nuget="$_cli_root/dotnet nuget"
_outputdir="$srcdir"/artifacts
# if true, then within pipeline environment, in which case send logs there
# to be scooped
if [ -d "$APORTSDIR/logs" ]; then
	_logdir="$APORTSDIR"/logs/Debug
else
	_logdir="$srcdir"/logs
fi

# generates tarball containing all components built by dotnet
snapshot() {
	local _pkg="$srcdir"/${builddir##*/}.tar
	ulimit -n 4096
	if [ -d "$srcdir" ]; then
		cd "$srcdir"
	else
		mkdir -p "$srcdir" && cd "$srcdir"
	fi
	if [ -d "installer" ]; then
		cd "$srcdir"/installer
	else
		git clone https://github.com/dotnet/installer && cd "$srcdir"/installer
	fi
	git checkout $_gittag

	sed 's|/src/installer||' "$startdir"/installer_2780-reprodicible-tarball.patch | patch -Np1 || true
	sed 's|/src/installer||' "$startdir"/installer_fix-version.patch | patch -Np1 || true
	if [ ! -d "$_cli_root" ]; then
		local _cli_root=
	fi

	_InitializeDotNetCli="$_cli_root" DOTNET_INSTALL_DIR="$_cli_root" DotNetBuildFromSource=true ./build.sh \
		/p:ArcadeBuildTarball=true \
		/p:TarballDir=$builddir \
		/p:TarballFilePath=$_pkg

	msg "Compressing ${builddir##*/}.tar to $SRCDEST"
	xz -T0 -9 -vv -e -c > "$SRCDEST"/${builddir##*/}.tar.xz < "$_pkg"

	cd "$startdir"
	abuild checksum
}

prepare() {
	default_prepare

	# adjusts sdk version to expected
	sed "s|7.0.100-rc.1.22431.12|$_bootstrapver|" -i "$builddir"/src/sdk/global.json
	sed "s|7.0.100-rtm.22478.12|$_bootstrapver|" -i "$builddir"/src/aspnetcore/global.json

	for i in runtime sdk installer aspnetcore roslyn; do
		sed "s|@@PACKAGESDIR@@|$_packagesdir|" -i "$builddir"/src/$i/NuGet.config
	done

	# fix sdk toolset download
	sed -i 's|$properties == \*"ArcadeBuildFromSource=true"\*|"${DotNetBuildFromSource:-}" == "true"|' "$builddir"/src/sdk/eng/restore-toolset.sh

	mkdir -p "$_cli_root"
	mkdir -p $_packagesdir $_downloaddir $_outputdir $_nugetdir $_logdir

	# links logfiles to pipeline logs for easy pickup in pipelines
	mkdir -p "$_logdir" "$builddir"/artifacts
	ln -s "$_logdir" "$builddir"/artifacts/logs
	ln -s "$_logdir" "$builddir"/artifacts/log
	for i in "$builddir"/src/*; do
		if [ -f "$i" ]; then
			continue
		fi
		mkdir -p "$_logdir"/${i##*\/} "$builddir"/src/${i##*\/}/artifacts
		ln -s "$_logdir"/${i##*\/} "$builddir"/src/${i##*\/}/artifacts/log
	done

	tar --use-compress-program="pigz" -xf "$srcdir"/dotnet-sdk-$_pkgver_macro*$_dotnet_arch* -C "$_cli_root" --no-same-owner

	for i in $_extra_nupkgs; do
		$_nuget push "$srcdir"/${i##*/} --source="$_packagesdir"
	done
}

_runtime() {
	"$_cli_root"/dotnet build-server shutdown
	if [ -z "${_runtimever+x}" ]; then
		local _runtimever=$(grep OutputPackageVersion "$builddir"/git-info/runtime.props | sed -E 's|</?OutputPackageVersion>||g' | tr -d ' ')
	fi
	local _runtimever_ns=$(awk '{if($2 ~ "Name=\"VS.Redist.Common.NetCore.SharedFramework.x64.*\""){print $3}}' "$builddir"/src/installer/eng/Version.Details.xml | awk -F '"' '{print $2}')

	msg "[$(date)] Building runtime version $_runtimever"
	cd "$builddir"/src/runtime

	local args="
		-c Release
		-bl
		-clang
		-arch $_dotnet_target
		/consoleLoggerParameters:ShowTimestamp
		/p:NoPgoOptimize=true
		/p:EnableNgenOptimization=false
		/p:GitCommitHash=$(cat ./.git/HEAD)
		"
	if [ "$CBUILD" != "$CHOST" ]; then
		local args="$args -cross"
	fi
	if [ "$_runtimever" != "${_runtimever##*-}" ]; then
		local args="$args /p:VersionSuffix=${_runtimever##*-}"
	fi
	ROOTFS_DIR="$CBUILDROOT" ./build.sh $args

	for i in artifacts/packages/*/*/*.nupkg; do
		$_nuget push $i --source="$_packagesdir"
	done
	mkdir -p "$_downloaddir"/Runtime/$_runtimever_ns
	cp artifacts/packages/*/*/dotnet-runtime-*-*.tar.gz $_downloaddir/Runtime/$_runtimever_ns
}

_roslyn() {
	"$_cli_root"/dotnet build-server shutdown
	if [ -z "${_roslynver+x}" ]; then
		local _roslynver=$(grep OutputPackageVersion "$builddir"/git-info/roslyn.props | sed -E 's|</?OutputPackageVersion>||g' | tr -d ' ')
	fi
	local _roslynver_ns=$(awk '{if($2 == "Name=\"Microsoft.Net.Compilers.Toolset\""){print $3}}' "$builddir"/src/installer/eng/Version.Details.xml | awk -F '"' '{print $2}')

	msg "[$(date)] Building roslyn version $_roslynver"
	cd "$builddir"/src/roslyn

	local args="
		-c Release
		-bl
		/consoleLoggerParameters:ShowTimestamp
		/p:GitCommitHash=$(cat ./.git/HEAD)
		"
	if [ "$_roslynver" != "${_roslynver##*-}" ]; then
		local args="$args /p:VersionSuffix=${_roslynver##*-}"
	fi
	DotNetBuildFromSource=false ./eng/build.sh --restore $args /p:UseAppHost=false
	./eng/build.sh --restore --build --pack $args
	for i in artifacts/packages/*/*/*.nupkg; do
		$_nuget push $i --source="$_packagesdir"
	done
}

_sdk() {
	"$_cli_root"/dotnet build-server shutdown
	if [ -z "${_sdkver+x}" ]; then
		local _sdkver=$(grep OutputPackageVersion "$builddir"/git-info/sdk.props | sed -E 's|</?OutputPackageVersion>||g' | tr -d ' ')
	fi
	local _sdkver_ns=$(awk '{if($2 == "Name=\"Microsoft.NET.Sdk\""){print $3}}' "$builddir"/src/installer/eng/Version.Details.xml | awk -F '"' '{print $2}')

	msg "[$(date)] Building sdk version $_sdkver"
	cd "$builddir"/src/sdk

	local args="
		-c Release
		-bl
		/consoleLoggerParameters:ShowTimestamp
		/p:GitCommitHash=$(cat ./.git/HEAD)
		/p:Architecture=$_dotnet_target
		"
	if [ "$_sdkver" != "${_sdkver##*-}" ]; then
		local args="$args /p:VersionSuffix=${_sdkver##*-}"
	fi

	# ArgumentsReflector doesn't build correctly when built from source
	DotNetBuildFromSource=false dotnet build src/Tests/ArgumentsReflector/ArgumentsReflector.csproj $args
	./build.sh --pack $args

	for i in artifacts/packages/*/*/*.nupkg; do
		$_nuget push $i --source="$_packagesdir"
	done
	mkdir -p "$_downloaddir"/Sdk/$_sdkver_ns
	cp artifacts/packages/*/*/dotnet-toolset-internal-*.zip "$_downloaddir"/Sdk/$_sdkver_ns
}

_aspnetcore() {
	"$_cli_root"/dotnet build-server shutdown
	if [ -z "${_aspnetver+x}" ]; then
		local _aspnetver=$(grep OutputPackageVersion "$builddir"/git-info/aspnetcore.props | sed -E 's|</?OutputPackageVersion>||g' | tr -d ' ')
	fi
	local _aspnetver_ns=$(awk '{if($2 == "Name=\"Microsoft.AspNetCore.App.Ref.Internal\""){print $3}}' "$builddir"/src/installer/eng/Version.Details.xml | awk -F '"' '{print $2}')

	msg "[$(date)] Build aspnetcore version $_aspnetver"
	cd "$builddir"/src/aspnetcore
	local args="
		-c Release
		-bl
		--os-name linux-musl
		-arch $_dotnet_target
		-no-build-nodejs
		/consoleLoggerParameters:ShowTimestamp
		/p:BuildNodeJs=false
		/p:GitCommitHash=$(cat ./.git/HEAD)
		/p:DotNetAssetRootUrl=file://$_downloaddir/
		/p:EnablePackageValidation=false
		"
	if [ "$_dotnet_target" = "x86" ] || [ "$_dotnet_target" = "ppc64le" ]; then
		local args="$args /p:CrossgenOutput=false"
	fi
	if [ "$_aspnetver" != "${_aspnetver##*-}" ]; then
		local args="$args /p:VersionSuffix=${_aspnetver##*-}"
	fi
	./eng/build.sh --pack $args

	for i in artifacts/packages/*/*/*.nupkg; do
		$_nuget push $i --source="$_packagesdir"
	done
	mkdir -p "$_downloaddir"/aspnetcore/Runtime/$_aspnetver_ns
	cp artifacts/installers/*/aspnetcore-runtime-internal-*-linux-musl-$_dotnet_target.tar.gz "$_downloaddir"/aspnetcore/Runtime/$_aspnetver_ns
	cp artifacts/installers/*/aspnetcore_base_runtime.version "$_downloaddir"/aspnetcore/Runtime/$_aspnetver_ns
}

_installer() {
	"$_cli_root"/dotnet build-server shutdown
	msg "[$(date)] Building installer version $_installerver"
	cd "$builddir"/src/installer

	local args="
		-c Release
		-bl
		/consoleLoggerParameters:ShowTimestamp
		/p:OSName=linux-musl
		/p:HostOSName=linux-musl
		/p:Architecture=$_dotnet_target
		/p:CoreSetupBlobRootUrl=file://$_downloaddir/
		/p:DotnetToolsetBlobRootUrl=file://$_downloaddir/
		/p:GitCommitHash=$(cat ./.git/HEAD)
		/p:GitCommitCount=$(grep GitCommitCount "$builddir"/git-info/installer.props | sed -E 's|</?GitCommitCount>||g' | tr -d ' ')
		/p:PublicBaseURL=file://$_downloaddir/
		"
	if [ "$_installerver" != "${_installerver##*-}" ]; then
		local args="$args /p:VersionSuffix=${_installerver##*-}"
	fi
	if [ "$_dotnet_target" = "x86" ]; then
		local args="$args /p:DISABLE_CROSSGEN=True"
	fi
	./build.sh $args

	mkdir  -p "$_downloaddir"/installer/$_installerver
	cp artifacts/packages/*/*/dotnet-sdk-$_pkgver_macro*.tar.gz "$_downloaddir"/installer/$_installerver
}

build() {
	export _InitializeDotNetCli=$_cli_root
	export DOTNET_INSTALL_DIR=$_cli_root
	export PATH="$_cli_root:$PATH"
	export NUGET_PACKAGES=$_nugetdir
	export DotNetBuildFromSource=true
	export DOTNET_CLI_TELEMETRY_OPTOUT=true
	export DOTNET_SKIP_FIRST_TIME_EXPERIENCE=true
	export SHELL=/bin/bash
	export EXTRA_CPPFLAGS="${CPPFLAGS/--sysroot=$CBUILDROOT}"
	export EXTRA_CXXFLAGS="${CXXFLAGS/--sysroot=$CBUILDROOT}"
	export EXTRA_CFLAGS="${CFLAGS/--sysroot=$CBUILDROOT}"
	export EXTRA_LDFLAGS="$LDFLAGS"
	unset CXXFLAGS CFLAGS LDFLAGS CPPFLAGS

	ulimit -n 4096

	# Parallel restore is broken on mono-based builds since dotnet7
	# see https://github.com/dotnet/runtime/issues/77364
	# A workaround via runtime_77364-mono-createdirectory-workaround.patch
	# removes the error, but env var is added in case
	case $CARCH in
		s390x|ppc64le) export RestoreDisableParallel=true CopyRetryCount=10;;
	esac

	_runtime
	_roslyn
	_sdk
	_aspnetcore
	_installer
}

package() {
	# lua-aports / buildrepo doesn't know to always build stage0 first when dealing
	# with virtual packages. Thus, we need to depend on an empty stage0 pkg that
	# dotnetx-build will pull, thus forcing build of stage0 first
	mkdir -p "$pkgdir"
}

bootstrap() {
	# allows stage0 to be pulled by dotnetx-build if first build of dotnetx
	provides="dotnet$_pkgver_name-bootstrap"
	provider_priority=$_pkgver_prior

	install -dm 755 \
		"$subpkgdir"/$_libdir/dotnet/bootstrap/$pkgver/docs \
		"$subpkgdir"/$_libdir/dotnet/bootstrap/$pkgver/comp \
		"$subpkgdir"/$_libdir/dotnet/artifacts/$pkgver

	# unpack build artifacts to bootstrap subdir for use by future builds
	tar --use-compress-program="pigz" \
		-xf "$_downloaddir"/installer/$_installerver/dotnet-sdk-$_pkgver_macro*.tar.gz \
		-C "$subpkgdir"/$_libdir/dotnet/bootstrap/$pkgver/ \
		--no-same-owner

	local _iltoolsArray="
		runtime.*.Microsoft.NETCore.TestHost.*.nupkg
		runtime.*.Microsoft.NETCore.ILAsm.*.nupkg
		runtime.*.Microsoft.NETCore.ILDAsm.*.nupkg
		"

	local _nupkgsArray="
		$_iltoolsArray
		Microsoft.NETCore.App.Host.*.*.nupkg
		Microsoft.NETCore.App.Runtime.*.*.nupkg
		Microsoft.NETCore.App.Crossgen2.*.*.nupkg
		runtime.*.Microsoft.NETCore.DotNetHost.*.nupkg
		runtime.*.Microsoft.NETCore.DotNetHostPolicy.*.nupkg
		runtime.*.Microsoft.NETCore.DotNetHostResolver.*.nupkg
		runtime.*.Microsoft.NETCore.DotNetAppHost.*.nupkg
		Microsoft.AspNetCore.App.Runtime.linux-musl-*.*.nupkg
		"

	# copies artifacts to artifacts dir for use by future dotnet builds
	for i in $_nupkgsArray; do install -Dm644 "$_packagesdir"/$i "$subpkgdir"/$_libdir/dotnet/artifacts/$pkgver/ || true; done
	for i in $_extra_nupkgs; do install -Dm644 "$srcdir"/${i##*/} "$subpkgdir"/$_libdir/dotnet/artifacts/$pkgver/; done

	msg "Changing iltools version to $_iltoolsver"
	# source-build expects a certain version of ilasm, ildasm and testhost
	# following adjusts version
	for i in $_iltoolsArray; do
		local nupath="$subpkgdir"$_libdir/dotnet/artifacts/$pkgver/$i
		local nupath=$(find $nupath || true)
		local nupkg="${nupath##*/}"
		local nuname="${nupkg/.nupkg}"
		if [ -z "${nuname/*rtm*}" ] || [ -z "${nuname/*servicing*}" ]; then
			nuname=${nuname%-*}
		fi
		local nuname="${nuname%.*.*.*}"
		local nuver="${nupkg/$nuname.}"
		local nuver="${nuver/.nupkg}"
		local nuspec="$nuname.nuspec"
		if [ ! "$nupath" ] || [ "$nupath" = "${nupath/$nuver/$_iltoolsver}" ]; then
			continue
		fi
		# shellcheck disable=SC2094
		unzip -p "$nupath" $nuspec | sed "s|$nuver|$_iltoolsver|" > "$srcdir"/$nuspec
		cd "$srcdir"
		zip -u "$nupath" $nuspec
		mv "$nupath" "${nupath/$nuver/$_iltoolsver}"
	done
}

# build relies on a plethora of nupkgs which are provided by this Artifacts file.
# stage0 sources these from Microsoft, which then allows bootstrap to build
# locally hosted versions. The following unpacks built tarball into directory
# for use by future builds.
artifacts() {
	pkgdesc="Internal package for building .NET $_pkgver_macro Software Development Kit"
	# hack to allow artifacts to pull itself
	provides="dotnet$_pkgver_name-bootstrap-artifacts"
	provider_priority=$_pkgver_prior

	# directory creation
	install -dm 755 \
		"$subpkgdir"/$_libdir/dotnet/artifacts/$pkgver \
		"$subpkgdir"/usr/share/licenses

	# extract artifacts to artifacts dir for use by future dotnet builds
	tar --use-compress-program="pigz" \
		-xf "$srcdir"/Private.SourceBuilt.Artifacts.*.noextract \
		-C "$subpkgdir"/$_libdir/dotnet/artifacts/$pkgver/ \
		--no-same-owner \
		--exclude '*x64*'
}

sha512sums="
1f0e7e6efb7076bc7d5a11e0589c27258cfe2bfa6d81a6d41f172c7292dab60f62b3e95e3873d4ee024d765f09507851d169a35ae06e75ce815d76717adb5025  dotnet-v7.0.100-rtm.22521.12.tar.xz
2ee0a055a3e46c6d9ced3cada5f91141b3966e76f4c4b11e58cd4c89ea69408a5b0efaaa21aaa04f743add38f1435f5a5852271a4222d5cd858907ec44f0af2e  dotnet-sdk-7.0.100-linux-musl-x64.noextract
8b3e95cc3e80eb05c7a0bc7ede1033320e03c78f4ecb7cc99b85fa99f56d72bc06342f342fe957e4aadacf9fb83fff15e658ae62c8fa8b29051898929c5ea833  dotnet-sdk-7.0.100-linux-musl-arm64.noextract
26b7ca079c8c2bafb32b5794de698dd325837897ee4120d0a7bbbcdc7f034de5031c6f30536866ccd7d1338625f937f41cfa5524c64163faded58846bfa674af  dotnet-sdk-7.0.100-linux-musl-arm.noextract
c61eead91329015d1cc2f73846cf080d1959e0906033b0a9a19839c495e356a493bef6e604d8f5a9700dd4f20edb412733dd34c099e5e5ba018df2c1901ed616  dotnet-sdk-7.0.100-linux-musl-s390x-2.noextract
0a45c2e01309de656e448f84f32d67cc55cfbec750a3d3eadb052096e32e9269a1e78a3b04d627f791c4b976dc1d0c40a031ef80320d0f17d33b54a4929c2a40  dotnet-sdk-7.0.100-linux-musl-ppc64le.noextract
cafb24303cdf92cdc8e2c703aa81684001fecf4a6f609a8008fd32ded9e06552bc04e38e21b6c26b0cf899686a9d05f6fad26a98bc73bd5d6347d682eec275a8  Private.SourceBuilt.Artifacts.7.0.100.noextract
2ede8d9352a51861a5b2550010ff55da8241381a6fa6cc49e025f1c289b230b8c0177e93850de4ea8b6f702c1f2d50d81a9f4d890ca9441c257b614f2a5e05dd  stylecop.analyzers.1.2.0-beta.435.nupkg
4394debb3cfec7bec7f223029c8283fd57b28ad765d47f354c7ca667756cbfc1ae4a51b216a0f81452400f60b58f08347bac327cd717a1947dad737b37cbe3ef  aspnetcore_use-linux-musl-crossgen-on-non-x64.patch
97d3194f8ad96521002e3693261451154d6ba567ea807e9daa4d55e1cda0693791fab4443e3d0d0fa8bc51ba53dd9b4077c723fa8f1f34cdd1661df828ce4227  build_set-local-repo.patch
0215d0fdbb4f46ab1cab547076cff39ccbe3e6ef0fdd26a60a562c60e1c022ee14286692a3880d75c665410a3db137f817e04c8c311a3960f8dede0036488741  installer_2780-reprodicible-tarball.patch
1fe84c55f63c3b32fe7c58aa23ba25e2e9ddeee21acd664947206b8a9678bf38e2c1a95252243d7981dd9423bda49f40dd463fa6e619fe7f329a31794829ee88  installer_runtimepacks.patch
ead82bb2276acc273d661e42597c76661984143bb7736cd6a64241f51b2da3b9b8625b6232e24c7f9a6965436f65a5b7bf1ba7669a01e6b9774ebd3a62f7ebe0  roslyn_allow-extra-params.patch
bbb97954f2fef5cfbb5b10b9396987bc525e0bba8215636892f7d6108d992710c4af4c159fa312c7841edeb4ab517033ae8231ce888296370646f17933777520  runtime_76500-mono-musl-support.patch
24f3fed752922d2ca3c151f21fedf6a257c7c74a155105a1aa09cdc847ba24e496212cb2d8548b134a8901c7da8d78c27fa93579211f7bdeadc598f808fed4a8  runtime_76500-properly-set-toolchain-for-alpine.patch
7e1834d7917bdfe330e92c9c658c4cfeb0926956799e389990a9fcd2fba21bbaacce41d27af089c3d70613f829331e9071dec00ca9ec0f88b2516b79c57a20a0  runtime_77269-fix-opcheckthis.patch
169ccf375696e82017b8d67dbcf6deb8d09c1f89681bc49b7ead699d45587a2bd407c1675cd14ad5a8701991312e14074af339bec58049a8ed9432fa851871cd  runtime_77270-fixed-fsharp-crash-issue.patch
d6ed172723915c0b67aa5153e34d52c002432ca0cada9e02a2a1975c3fd9cffd3ba24e4ab1d6b9866d91f964598b79c7876528a5f2f5a7b4ffb376c2d601c8b0  runtime_77364-mono-createdirectory-workaround.patch
f6d6785815d87072784354b5b962ad1dd2e713c534976de953b970d086afb6f166da01237b2767a62dc97ced9af3b6f90a21d6d08e6c6487bcd5871715255a22  runtime_77682-mono-cgroup-min-memory.patch
896ab17e3ecb36b80c6f7a6790c583655f8e82d9dda4062f7911952880ff41f483af16a64a0e1645cd6ee508a1f21578ddfc1e0ab43998b690e9e92324126283  runtime_78146-fixed-clang15-build-issue.patch
2ff8b42bce25b8389177bb6389bfdc796c104c5d992e72a6e7c8af0cda8f70f26f480071db380df78db745eee72e4343220f79ec6b22aa880fa2531eda8c8593  runtime_make-lld-use-depend-on-existing-on-target.patch
8be4b028fb15d4bc0ba34711f7491c49b4908e017a9c46fcd3817c59aa1459c0217dca405f46cfaf5e115ad0fbcd60d2593c05c6e1da2632612bf6179867cd10  runtime_non-portable-distrorid-fix-alpine.patch
b5f28aa8bdbd24c589276a60893859965eb78b0abfbdac7a6e86715ac87e3bbd8bc271852ff62a41ea83b53af02e1c1814abcccc8862c07b3ac74633d1be0a32  sdk_dummyshim-fix.patch
"
