# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kpat
pkgver=22.12.0
pkgrel=0
pkgdesc="KPatience offers a selection of solitaire card games"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/games/kpat/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	black-hole-solver-dev
	extra-cmake-modules
	freecell-solver-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kguiaddons-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kpat-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
# Broken tests
options="!check"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
707b7fcb0909438938e18a852cb1aa6065a189a7536e62fe9e4bbd3cb812ae9f4dc6f4cbf776d07c7eb08af2d0975cfb056c9528ad869817404d4f932bdc8587  kpat-22.12.0.tar.xz
"
