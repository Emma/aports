# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=diffoscope
pkgver=228
pkgrel=0
pkgdesc="In-depth comparison of files, archives, and directories"
url="https://diffoscope.org/"
arch="noarch"
license="GPL-3.0-or-later"
depends="
	py3-libarchive-c
	py3-magic
	python3
	"
makedepends="
	py3-docutils
	py3-setuptools
	python3-dev
	"
checkdepends="
	bzip2
	cdrkit
	gzip
	libarchive-tools
	openssh-client-default
	py3-html2text
	py3-pytest
	unzip
	"
source="https://salsa.debian.org/reproducible-builds/diffoscope/-/archive/$pkgver/diffoscope-$pkgver.tar.gz"

build() {
	python3 setup.py build
}

check() {
	# html test fails
	PYTHONPATH="$PWD" \
	PYTHONDONTWRITEBYTECODE=1 \
	pytest -v -k 'not test_diff'
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
700698316a80953848767b9c93d33e125a423837512ecd679b3513525c273d249240d9c3b19498b5246c47e1c173ad43bdcb46dde295298f01025a8293ec620b  diffoscope-228.tar.gz
"
