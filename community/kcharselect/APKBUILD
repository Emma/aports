# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kcharselect
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.kcharselect"
pkgdesc="A tool to select special characters from all installed fonts and copy them into the clipboard"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kbookmarks-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcharselect-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
56f7a2506b19fcd89bc37e3ecd7b037798c0a4118a89cf72db3a728bf24ce0e9c45d17e294c752c2767bdcec9f8b422b600ac335e551dc429f8a38894301ca49  kcharselect-22.12.0.tar.xz
"
