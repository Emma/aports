# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kmag
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.kmag"
pkgdesc="A screen magnifier"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdoctools-dev
	ki18n-dev
	kio-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmag-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
612cc57f34acc928c794a57607212f772f1a748e486ab9fe6cf54c4d98191395e1ee38354665e325524349ef174fa699e8e8eda80516cb658184e136b57353c6  kmag-22.12.0.tar.xz
"
