# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=py3-inquirer
pkgver=3.1.0
pkgrel=0
pkgdesc="collection of common interactive command line user interfaces"
url="https://github.com/magmax/python-inquirer"
arch="noarch"
license="MIT"
depends="python3 py3-blessed py3-readchar py3-python-editor"
makedepends="py3-poetry-core py3-gpep517 py3-installer py3-wheel"
checkdepends="py3-flake8 py3-pexpect py3-pytest py3-pytest-cov py3-pytest-xdist
	py3-mock py3-nosexcover py3-coveralls py3-wheel ncurses-terminfo"
# GH tarballs required for tests!
source="$pkgname-$pkgver.tar.gz::https://github.com/magmax/python-inquirer/archive/v$pkgver.tar.gz"
builddir="$srcdir/python-inquirer-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	TERM=xterm-256color PYTHONPATH=src pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/inquirer-$pkgver-*.whl
}

sha512sums="
b5490c81746682091c0a7b014666f05c66404fe977a0242d84acad9c9e97c7af31c5b8ab0cbe607a96c442786216ee1e95f6dc272be909b1e79b9f6ceba29576  py3-inquirer-3.1.0.tar.gz
"
