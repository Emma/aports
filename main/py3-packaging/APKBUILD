# Contributor: Breno Leitao <breno.leitao@gmail.com>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=py3-packaging
_pkgname=packaging
pkgver=22.0
pkgrel=0
pkgdesc="Core utilities for Python3 packages"
options="!check" # Requires py3-pytest which requires py3-setuptools
url="https://pypi.python.org/pypi/packaging"
arch="noarch"
license="Apache-2.0 AND BSD-2-Clause"
depends="python3 py3-parsing"
makedepends="py3-flit-core py3-gpep517"
# disable check to break circular dep with py3-setuptools
checkdepends="py3-pytest py3-pretend"
source="https://files.pythonhosted.org/packages/source/p/packaging/packaging-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-packaging" # Backwards compatibility
provides="py-packaging=$pkgver-r$pkgrel" # Backwards compatibility

[ "$CARCH" = s390x ] && options="$options !check" # fails a lot

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

check() {
	PYTHONPATH="$PWD/build/lib" python3 -m pytest \
		--ignore=tests/test_manylinux.py \
		--ignore=tests/test_markers.py \
		--ignore=tests/test_specifiers.py \
		--ignore=tests/test_tags.py \
		--ignore=tests/test_version.py
	# tests that require https://pypi.org/project/pretend/ are disabled
	# above because py3-pretend hasn't been packaged for Alpine Linux
}

sha512sums="
5ede6c50da305828c7a5e56eb820f23c1cc232b82e36e7db1075983b8e2576515c15370ccdf87dce7afb95cf40e9716736ad0e2c2ba917884ccd315016496f87  packaging-22.0.tar.gz
"
