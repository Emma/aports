# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-dpath
_pkgname=dpath
pkgver=2.1.3
pkgrel=0
pkgdesc="Filesystem-like pathing and searching for dictionaries"
url="https://github.com/dpath-maintainers/dpath-python"
arch="noarch"
license="MIT"
replaces="py-dpath" # for backwards compatibility
provides="py-dpath=$pkgver-r$pkgrel" # for backwards compatibility
depends="python3"
makedepends="py3-setuptools"
checkdepends="py3-hypothesis py3-mock py3-nose2 py3-pytest"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"

builddir="$srcdir/$_pkgname-$pkgver"

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH="$builddir"/build/lib pytest-3
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
3bf56be1375d7e44ffa29172b75348d8bdf8012d4f95d01d62367dc95ec06e96aeba451268067de70c81bc28998e612b4760442a1f9d433cd479e8d3c0a81719  dpath-2.1.3.tar.gz
"
