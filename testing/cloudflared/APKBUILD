# Contributor: Piper McCorkle <piper@cloudflare.com>
# Maintainer: Piper McCorkle <piper@cloudflare.com>
pkgname=cloudflared
pkgver=2022.11.1
pkgrel=1
pkgdesc="Cloudflare Tunnel client"
url="https://github.com/cloudflare/cloudflared"
arch="aarch64 x86 x86_64"
license="Apache-2.0"
makedepends="go gettext"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/cloudflare/cloudflared/archive/refs/tags/$pkgver.tar.gz
	goflags.patch
	"
options="!check" # require privileged icmp sockets

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"
export GOFLAGS="$GOFLAGS -trimpath"

build() {
	make cloudflared
	DATE="\"$(date -u '+%Y-%m-%d-%H%M UTC')\"" VERSION="$pkgver" envsubst < cloudflared_man_template > cloudflared.1
}

check() {
	_goarch=$(go tool dist env | grep GOARCH | sed 's/^GOARCH="//; s/"$//')
	# Go race detector is only supported on amd64, ppc64le, and arm64
	if [ $_goarch = "amd64" ] || [ $_goarch = "ppc64le" ] || [ $_goarch = "arm64" ]; then
		_race=-race
	fi
	go test -mod=vendor -buildmode=default $_race ./...
}

package() {
	install -D -m755 ./cloudflared "$pkgdir"/usr/bin/cloudflared
	install -D -m644 ./cloudflared.1 "$pkgdir"/usr/share/man/man1/cloudflared.1
}

sha512sums="
e6abe196fc20b786e4611e15253d9b69b6a7a9a83e6862977da7e81891ebfe16bb2926de7fe03946a55cbe76106c3401028d87fbfce8c938ccb6e76f3468ed73  cloudflared-2022.11.1.tar.gz
cdec4eb5a5735af0b472acaddbcc3e1030ec26005701da977c1dab6e0d897b6e7661caf9f628691c960c6435b725facf3b18648ee3dbacac0c7c67afcff2fd13  goflags.patch
"
