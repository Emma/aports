# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=onevpl
pkgver=2023.1.0
pkgrel=0
pkgdesc="oneAPI Video Processing Library"
url="https://github.com/oneapi-src/oneVPL"
arch="x86_64" # only x86_64 supported
license="MIT"
makedepends="cmake samurai"
subpackages="$pkgname-doc $pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/oneapi-src/oneVPL/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/oneVPL-$pkgver"

build() {
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DONEAPI_INSTALL_ENVDIR=/usr/share/oneVPL/env \
		-DONEAPI_INSTALL_LICENSEDIR=/usr/share/doc/oneVPL \
		-DONEAPI_INSTALL_MODFILEDIR=/usr/share/oneVPL/modulefiles \
		-DBUILD_PREVIEW=OFF \
		-DBUILD_EXAMPLES=OFF \
		-DBUILD_TOOLS=OFF \
		-DINSTALL_EXAMPLE_CODE=OFF \
		-DBUILD_TESTS="$(want_check && echo ON || echo OFF)"
	cmake --build build
}

check() {
	ctest -j $JOBS --output-on-failure --test-dir build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

dev() {
	default_dev

	amove usr/share
}

sha512sums="
a8e1f3a1753dbefc9cd4b38df9aac6048181806496677e34330012aaa62be690ef322a34c949a2fdada7909e62fc552d60a382ed75bd3ce6e079b3511a62d335  onevpl-2023.1.0.tar.gz
"
